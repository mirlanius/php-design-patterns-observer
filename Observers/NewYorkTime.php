<?php

require_once('./Observers/TimeFormat.php');

class NewYorkTime implements IObserver {

    private $timezone = 'America/New_York';

    public function update(SubjectInfo $subjectInfo)
    {
        $time_format = new TimeFormat;
        echo $time_format($this->timezone, $subjectInfo->unixTime);
    }
}