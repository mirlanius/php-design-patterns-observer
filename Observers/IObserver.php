<?php
require_once("./Observers/IObserver.php");

interface IObserver {
    public function update(SubjectInfo $subjectInfo);
}