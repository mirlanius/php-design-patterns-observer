<?php
class TimeFormat {

    /**
     * Возвращает форматированную строку со значением времени
     */
    public function __invoke($timezone_string, $timestamp):string
    {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('UTC'));
        $date->setTimestamp($timestamp);
        $date->setTimezone(new DateTimeZone($timezone_string));

        $time_string  = $timezone_string." ". $date->format('d-m-y h:i:s'). PHP_EOL;
        return $time_string;
    }

}