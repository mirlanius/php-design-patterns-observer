<?php
require_once("./Observers/IObserver.php");

class MoscowTime implements IObserver {
    private $timezone = 'Europe/Moscow';

    public function update(SubjectInfo $subjectInfo)
    {
        $time_format = new TimeFormat;
        echo $time_format($this->timezone, $subjectInfo->unixTime);
    }
}