<?php
require_once("./Observers/IObserver.php");

class JapanTime implements IObserver {
    private $timezone = 'Asia/Tokyo';

    public function update(SubjectInfo $subjectInfo)
    {
        $time_format = new TimeFormat;
        echo $time_format($this->timezone, $subjectInfo->unixTime);
    }
}