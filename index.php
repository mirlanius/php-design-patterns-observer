<?php
require("./Observers/MoscowTime.php");
require("./Observers/JapanTime.php");
require("./Observers/NewYorkTime.php");
require("./Subject/TimerSubject.php");

$subject = new TimerSubject();
$observerMoscow = new MoscowTime();
$observerJapan = new JapanTime();
$observerNewYork = new NewYorkTime();

$subject->attach($observerMoscow);
$subject->attach($observerJapan);
$subject->attach($observerNewYork);


//обновляем данные субьекта и извещаем слушателей
$subjectInfo = new SubjectInfo();
$subjectInfo->unixTime = time();
$subject->updateState($subjectInfo);
echo PHP_EOL;

//удаляем один из слушателей и повторно обновляем событие
echo "detach Tokyo", PHP_EOL;

$subject->detach($observerJapan);
$subject->updateState($subjectInfo);