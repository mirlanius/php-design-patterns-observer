<?php

require_once("./Subject/ISubject.php");
require_once("./Subject/SubjectInfo.php");

/**
 * Класс Субъекта (реализуется без использования страндартной библиотеки интерфейсов)
 * Является источником данных для всех Обсерверов
 */
class TimerSubject implements ISubject {

    private array $observers = [];
    private SubjectInfo $info;

    public function __construct(){
        $this->info = new SubjectInfo();
        date_default_timezone_set('UTC');
    }

    /**
     * Добавление обсервера
     */
    public function attach(IObserver $observer) {
        $this->observers[spl_object_id($observer)]= $observer;
    }

    /**
     * Удаление обсервера
     */
    public function detach(IObserver $observer) {
        if(isset($this->observers[spl_object_id($observer)]) === false){
            return;
        }
        unset($this->observers[spl_object_id($observer)]);
    }


    /** 
     * Обновление данных Субъекта
     */
    public function updateState(SubjectInfo $info){
        $this->info = $info;
        $this->notify();
    }

    /**
     * Оповещение всех Обсерверов
     */
    public function notify() {

        if(count($this->observers) == 0){
            return;
        }

        /** @var IObserver $observer */
        foreach ($this->observers as $observer) {
            $observer->update($this->info);
        }
    }


}